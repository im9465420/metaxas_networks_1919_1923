# Metaxas_networks_1919_1923


## Name
Ioannis Metaxas' networks from 1919 to 1923. 

## Project Description

This project presents the datasets resulting from [my thesis](https://www.diva-portal.org/smash/record.jsf?aq2=%5B%5B%5D%5D&c=1&af=%5B%5D&searchType=SIMPLE&sortOrder2=title_sort_asc&query=Charikleia+Tsakiri&language=en&pid=diva2%3A1892916&aq=%5B%5B%5D%5D&sf=all&aqe=%5B%5D&sortOrder=author_sort_asc&onlyFullText=false&noOfRows=50&dswid=4179) at Linnaeus University in the Digital Humanities Master Program. The aim of my thesis was to explore the network of Ioannis Metaxas from his exile in Italy (1919-1920) until his return to Greece and subsequent second exile (1921-1923) using Social Network Analysis (SNA). The data was curated by reading and analyzing Metaxas' diaries from this period. 

The project includes two datasets: one detailing the nodes and the other the edges. The node dataset includes attributes such as gender, ethnicity, and occupation, while the edges dataset outlines the connections between these individuals and Metaxas, including timestamps that allow the production of temporal networks.  These datasets were specifically created for use in SNA and have been utilized in Gephi to generate temporal networks' visualizations (see the examples).

## Vizualization Examples

Examples of what can be vizualized with the data:

- Visualization of Metaxas' network from 1919 to 1923.
- Visualization of the distribution of males and females in the network (1919-1923).
- Visualization of the distribution of nationalities within the network (1919-1923).
- Visualization of the distribution of occupations among the network members (1919-1923).
- Community detection to identify distinct groups within Metaxas' network (1919-1923).

## Usage
Datasets can be used to explore Metaxas' personal network during the years 1919-1923 with any network analysis tool.

## Support
Feel free to ask me anything or propose changes at: clairetsakiris@hotmail.com

## License
CC BY-SA 4.0 International 

